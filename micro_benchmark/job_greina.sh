#!/bin/sh

# Execute by salloc -N8 -n8 bash run.sh > output.log

MACHINEFILE="nodes.$SLURM_JOB_ID"

# Generate Machinefile for mpich such that hosts are in the same
#  order as if run via srun
#
srun -l /bin/hostname | sort -n | awk '{print $2}' > $MACHINEFILE

echo "Start: $(date)"
echo "SLURM_NTASKS: $SLURM_NTASKS"
echo "-----------------------------"

bin=./run.out
processes=(8)
densities=(0.0625 0.03125 0.015625 0.0078125 0.00390625 0.001953125)
dimensions=(16777216)

for n in "${dimensions[@]}"; do
  for p in "${processes[@]}"; do
    for d in "${densities[@]}"; do
      cmd="mpirun -np $SLURM_NTASKS -machinefile $MACHINEFILE $bin $n $d"
      output="outputs_greina/output_${n}_${p}_[${d}].log"
      echo "Running: $cmd / Output: $output"
      $cmd &>$output
    done
  done
done

rm $MACHINEFILE

echo "-----------------------------"
echo "End: $(date)"
