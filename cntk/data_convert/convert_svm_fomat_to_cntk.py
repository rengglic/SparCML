import sys, os
import numpy as np;
import matplotlib.pyplot as plt

def process(filename):

    if not os.path.exists(filename):
        print("File '{0}' does not exists!".format(filename));
        return;

    output = os.path.splitext(filename)[0] + "_cntk.txt";
    print("Writing to file: " + output);

    with open(filename, 'r') as f:
        with open(output, 'w') as o:
            for line in f:
                vals = line.split(" ", 1);
                o.write("|features {0} |labels ".format(vals[1].strip()));
                if(vals[0] == "-1"):
                    o.write("0\n");
                else:
                    o.write("1\n");

if __name__ == '__main__':
    if (len(sys.argv) != 2):
        print("You have to specify a file!");
    else:
        process(sys.argv[1]);
