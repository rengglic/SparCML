#ifndef __COLUMN_QUANTIZER_H__
#define __COLUMN_QUANTIZER_H__
#include "ValueQuantizer.h"
#include <math.h>

#ifndef CPUONLY
#include <curand_kernel.h>
#include <ctime>
#endif // !CPUONLY

#pragma warning(disable : 4127) // conditional expression is constant

namespace Microsoft { namespace MSR { namespace CNTK {

#define ColMIDX(i, j, numRow) (((j) * (numRow)) + (i)) // 0 based indexing for column major

// ---------------------------------------------------------------------------
// Class to perform columnwise quantization/unquantization
//
// The quantization of a column is performed in 2 steps
// a) Compute the values used for unquantizing/reconstructing the quantized values. This is done by computing a pair of
//    values that specify the range of reconstructed unquantized values, such that the aggregate qunatization error is minimized.
// b) Perform the actual quantization by quantizing each value in the column to an integer of the size of
//    the specified number of bits and then packing these integer bits into the quantized matrix storage
// ---------------------------------------------------------------------------

template <class ElemType>
class ColumnQuantizer
{
    typedef typename ValueQuantizer<ElemType>::QWord QWord;
    typedef typename ValueQuantizer<ElemType>::QWordVal QWordVal;
    static const size_t QWordNumBits = ValueQuantizer<ElemType>::QWordNumBits;

public:
    cudacode ColumnQuantizer(size_t logNbits, ElemType scalingFactor)
        : valQ(logNbits, scalingFactor)
    {
    }

    // compute #QWords per column of a given height
    static size_t QWordsPerCol(size_t rows, size_t Nbits)
    {
        const size_t valsPerQWord = QWordNumBits / Nbits;
        return (rows + valsPerQWord - 1) / valsPerQWord;
    }

    size_t QWordsPerCol(size_t rows) const
    {
        return QWordsPerCol(rows, valQ.NBits());
    }

    // quantize a matrix column into qcoldata
    //  The current value of 'inResidual' is added to the matrix, and 'outResidual' gets updated with the new residual;
    //  inResidual = outResidual is allowed (intended)
    template <bool ZeroThresholdFor1Bit>
    cudacode void Quantize(const ElemType* inMat, const ElemType* inResidual, long M, size_t j, QWord* qColBits, ElemType* outResidual) const
    {
        // we loop over QWord values
        // E.g. there are 35 ints for a 1100-dim column (at 1-bit quantization).
        // For better CUDA memory collating, we interleave memory such that computing consecutive ints triggers consecutive memory accesses
        // (although for the CPU side, it breaks caching; we could do in-place op)
        // E.g., int  0 accesses elements 0, 35, 70, etc.
        // while int  1 accesses elements 1, 36, 71, etc
        // up to int 34 accesses elements 34, 69, 104, etc.
        const size_t numQWordsPerCol = QWordsPerCol(M);
        for (size_t iQWord = 0; iQWord < numQWordsPerCol; iQWord++)
        {
            qColBits[iQWord] = QuantizeOneQWord<ZeroThresholdFor1Bit>(inMat, inResidual, M, iQWord, M, numQWordsPerCol, j, outResidual);
        }
    }

    // unquantize a matrix column from qcoldata
    // If 'add' then add to the existing content of the matrix (this is a common thing to do; saves a buffer).
    cudacode void Unquantize(ElemType* outMat, long M, size_t j, const QWord* qColBits, bool add) const
    {
        // loop over QWord values
        const size_t numQWordsPerCol = QWordsPerCol(M);
        for (size_t iQWord = 0; iQWord < numQWordsPerCol; iQWord++)
        {
            UnquantizeOneQWord(outMat, M, iQWord, M, numQWordsPerCol, j, qColBits[iQWord], add);
        }
    }

    // workaround for not being able to declare a default argument for lambda parameters
    template <bool ZeroThresholdFor1Bit>
    static cudacode void ComputeRangeStatColj(const ElemType* inMat, const ElemType* inResidual, long M, size_t j, size_t bits, ElemType& scalingFactor)
    {
        /*dummy reducers do nothing in linear CPU version*/
        ComputeRangeStatColjSubset<ZeroThresholdFor1Bit>(inMat, inResidual, M, j, bits, scalingFactor, 0, 1, [](ElemType&){}, [](unsigned int&){});
    }

public:
    // quantize the value in  inMat[rowStart,colIdx],  inMat[rowStart + rowStride,colIdx],inMat[rowStart + rowStride*,colIdx]  ... and pack them into a QWord
    // Question: note that it is somewhat un-intuitional, but this memory access pattern is efficient for GPU?
    
    // randomSeed is set for GPU only from GPU kernel so we have different seed for different threads
    template <bool ZeroThresholdFor1Bit>
    cudacode QWord QuantizeOneQWord(
        const ElemType* inMat, const ElemType* inResidual,
        long M,
        size_t rowStart, size_t rowEnd, size_t rowStride,
        size_t colIdx,
        ElemType* outResidual,
        size_t randomSeed = 0) const
    {
        QWord bitBuf = 0;

#ifndef CPUONLY
        // structure for cuda random numbers
        curandState state;
        curand_init((unsigned long long)clock() + randomSeed, 0, 0, &state);
#else
        srand((unsigned long long)clock());
#endif

        // number of bits in a QWord
        size_t i = rowStart;
        for (size_t k = 0; (k < QWordNumBits) && (i < rowEnd); k += valQ.NBits(), i += rowStride)
        {
#ifndef CPUONLY
            // generate random number
            float randomNumber = curand_uniform(&state);
#else
            float randomNumber = rand() / (float) RAND_MAX;
#endif                       

            // quantize
            size_t ij = ColMIDX(i, colIdx, M);
            ElemType val = inMat[ij]; // + inResidual[ij];
            QWordVal qval = valQ.Quantize<ZeroThresholdFor1Bit>(val, randomNumber);

            // compute residual
            ElemType uval = valQ.Unquantize(qval);
            ElemType r = val - uval;
            outResidual[ij] = r;
            bitBuf = bitBuf | (qval << k);
        }

        return bitBuf;
    }

    // unquantize one QWord of a quantized matrix column
    cudacode void UnquantizeOneQWord(
        ElemType* us, long M,
        size_t rowStart, size_t rowEnd, size_t rowStride,
        size_t colIdx, QWord bitBuf, bool add) const
    {
        // (rangeend MUST be a power of two; ensured by constructing off ldNbits)
        const QWordVal bitmask = valQ.QuanRangeEnd() - 1;
        size_t i = rowStart;
        for (size_t k = 0; (k < QWordNumBits) && (i < rowEnd); k += valQ.NBits(), i += rowStride)
        {
            // get value
            const QWordVal qval = (bitBuf >> k) & bitmask; // % 2^Nbits

            // unquantize
            ElemType val = valQ.Unquantize(qval);
            size_t ij = ColMIDX(i, colIdx, M);
            if (add)
            {
                val += us[ij];
            }

            us[ij] = val;
        }
    }

    // determine quantization range of one column
    // This code is written so that it can run in parallel threads on CUDA for collated memory access;
    // set 'subsets' to >1 and pass cross-thread reducer functions for 'float' and 'size_t' (which would reduce through using CUDA __shared__ memory).
    // TODO: further opportunity for speed-up: use 'mean' from last round for 1-bit and stddev calc
    template <bool ZeroThresholdFor1Bit, class F1, class F2>
    static cudacode void ComputeRangeStatColjSubset(
        const ElemType* inMat,
        const ElemType* inResidual, long M,
        size_t j,
        size_t bits,
        ElemType& scalingFactor,
        size_t subset, size_t subsets,
        F1 allReduceElem, F2 allReduceUint,
        int realNumRows = -1)
    {
        size_t rows = realNumRows == -1 ? M : realNumRows;

        // we split interval [0, norm]
        // into 2 ^ (bits - 1) - 1 parts
        // so here we need just to calculate norm

        ElemType currNorm = 0.0f; // TODO: make this a parameter

        // (subset: compute subset sum)
        for (size_t i = subset; i < rows; i += subsets)
        {
            size_t ij = ColMIDX(i, j, M);
            ElemType val = inMat[ij]; // + inResidual[ij];
            currNorm += val * val;
        }
        // multi-subset (CUDA): reduce to one thread
        allReduceElem(currNorm);
        ElemType totalNorm = sqrt(currNorm);
        if (subset == 0)
        {
            scalingFactor = totalNorm;
        }
    }

    template <bool ZeroThresholdFor1Bit, class F1, class F2>
    static cudacode void ComputeRangeStatColjSubsetMax(
        const ElemType* inMat,
        const ElemType* inResidual, long M,
        size_t j,
        size_t bits,
        ElemType& scalingFactor,
        size_t subset, size_t subsets,
        F1 allReduceElem, F2 allReduceUint,
        int realNumRows = -1)
    {
        size_t rows = realNumRows == -1 ? M : realNumRows;

        // we split interval [0, max]
        // into 2 ^ (bits - 1) - 1 parts
        // so here we need just to calculate max

        ElemType currMax = 0.0f; // TODO: make this a parameter

        // (subset: compute subset sum)
        for (size_t i = subset; i < rows; i += subsets)
        {
            size_t ij = ColMIDX(i, j, M);
            ElemType val = inMat[ij]; // + inResidual[ij];
            currMax = max(currMax, abs(val));
        }
        // multi-subset (CUDA): reduce to one thread
        allReduceElem(currMax);
        if (subset == 0)
        {
            scalingFactor = currMax;
        }
    }

private:
    ValueQuantizer<ElemType> valQ;

    template <typename T>
    friend class QuantizedMatrix;
};

}}}
#endif
